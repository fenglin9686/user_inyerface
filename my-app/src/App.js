import React from 'react';
import LoginRoute from './Route/login'
import registerRoute from './Route/register'
import profileRoute from './Route/profile'
import './App.css';
import { BrowserRouter, Route} from 'react-router-dom'

function App() {
  return (
    <React.Fragment>
    <BrowserRouter>
        <Route exact path='/' component={LoginRoute}></Route>
        <Route path='/register' component={registerRoute}></Route>
        <Route path='/profile' component={profileRoute}></Route>
    </BrowserRouter>
</React.Fragment>
  );
}

export default App;
