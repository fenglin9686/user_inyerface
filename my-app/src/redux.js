const initialState = {
    UserId:""
}
//Reducer
const reducer = (state = { ...initialState }, action) => {

    switch (action.type) {
        case "LOGIN":
            return {
                userId: action.id
            }
        default:
            return {
                userId:""
            }
    }
}

export default reducer;