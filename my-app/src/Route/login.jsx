import React from 'react';
import axios from 'axios';  
import { withRouter, Link} from 'react-router-dom';
import { connect } from 'react-redux';
class login extends React.Component{
    constructor(props) {
        super(props)
        this.state={
            username:"",
            password:"",
            message:""
        }
        this.handleUserName=this.handleUserName.bind(this);
        this.handlePassword=this.handlePassword.bind(this);
    }
    handleUserName(event){
      this.setState({
        username:event.target.value
      })
    }
    handlePassword(event){
        this.setState({
            password:event.target.value
         })
    }
     fakeMessage(){
         const list=["Connection Error, Try to click login again", 
                "Invalid User and Password, Please fill in right password and click login again",
                "Bad Connect , Try to click login again"]
      this.setState({
        message: list[Math.floor((Math.random() * 3) + 0)]
      })
    }

    async login(){
       if(!this.state.username || !this.state.password){
           window.alert("Please fill in the all the filed")
           return;
       }
      
       const checkCred=await axios({
        method: 'post',
        url: 'http://localhost:9025/bycred',
        data:{
            "username": this.state.username,
            "userpassword": this.state.password
        },
        withCredentials: true,
         });
         console.log(checkCred.data);
         if(!checkCred.data){
            window.alert("User Name and password not match")
           return;
         }
        this.props.setId(checkCred.data.userid)
        this.props.history.push('/profile');
    }
    render(){
        return(
            <div className="form">
            <div class="form-group">
                <label for="exampleInputEmail1">User Name</label>
                <input  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" onChange={this.handleUserName} placeholder="Enter User Name" />
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"  onChange={this.handlePassword}/>
            </div>
            <button type="submit" class="btn btn-primary" onClick={()=>this.fakeMessage()}>Login</button>
            <Link to="/register"> <button type="submit" class="btn btn-primary">Register</button></Link>
            <div className="fakeName" onClick={()=>this.login()}>{this.state.message}</div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setId: (id) => dispatch({ type: 'LOGIN', id: id }),
    }
  }

export default connect(null, mapDispatchToProps)(withRouter(login))