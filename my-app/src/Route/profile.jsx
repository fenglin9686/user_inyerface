import React from 'react';
import axios from 'axios';  
import { withRouter, Link} from 'react-router-dom';
import { connect } from 'react-redux';
class profile extends React.Component{
    constructor(props) {
        super(props)
        if (!this.props.current_id) {
           this.props.history.push("/");
         }
         this.state={
             userinfo:null,
            showfield:false,
            username:"",
            password:"",
            email:"",
            first:"",
            last:"",
        }
        this.handleUsername=this.handleUsername.bind(this);
        this.handlePassword=this.handlePassword.bind(this);
        this.handleEmail=this.handleEmail.bind(this);
        this.handleFirst=this.handleFirst.bind(this);
        this.handleLast=this.handleLast.bind(this);
    }

    async componentDidMount(){   
        this.setState({
            userinfo: await this.getUserInfo(),
            showfield:true
        })
    }
   async getUserInfo(){
        const getUser=await axios({
            method: 'post',
            url: 'http://localhost:9025/byid',
            data:{
                "userid": this.props.current_id,
            },
            withCredentials: true,
         });
         console.log(getUser.data);
         return getUser.data;
    }
    handleUsername(event){
        this.setState({
            username:event.target.value
        }) 
    }
    handlePassword(event){
        this.setState({
            password:event.target.value
        })
    }
    handleEmail(event){
        this.setState({
            email:event.target.value
        })  
    }
    handleFirst(event){
        this.setState({
            first:event.target.value
        })  
    }
    handleLast(event){
        this.setState({
            last:event.target.value
        })
        
    }
    async update(){
        if(!this.state.username || !this.state.email || !this.state.first || !this.state.last || !this.state.password){
            window.alert("Please fill in all information")
            return;
        }
        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)){
            window.alert("invalide email format")
            return;
        }
        const checkUser=await axios({
            method: 'post',
            url: 'http://localhost:9025/byusername',
            data:{
                "username": this.state.username,
            },
            withCredentials: true,
             });
            if(checkUser.data && checkUser.data.userid!==1){
                window.alert("The username already exists")
                return;
            }
            await axios({
                method: 'put',
                url: 'http://localhost:9025/updateUser',
                data:{
                    "userid" : this.props.current_id,
                    "username": this.state.username,
                    "userpassword": this.state.password,
                    "firstname":this.state.first,
                    "lastname":this.state.last,
                    "email":this.state.email
                },
                withCredentials: true,
                 });
                 window.alert("Update successful!")
               
    }
    render() {
        if(!this.state.showfield){
            return(
                <div>Loading</div>
            )
        }
        return(
            <div className="form">
            <h1>Update User</h1>
            <div class="input-group mb-3">
             <div class="input-group-prepend">
                 <span class="input-group-text" id="basic-addon3">User Name</span>
            </div>
            <input type="text" onChange={this.handleUsername} value={this.state.username} placeholder={this.state.userinfo.username} class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
            </div>
           <div class="input-group mb-3">
             <div class="input-group-prepend">
                 <span class="input-group-text" id="basic-addon3">Email</span>
            </div>
            <input type="text"  onChange={this.handleEmail}  value={this.state.email} placeholder={this.state.userinfo.email} class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
            </div>
            <div class="input-group mb-3">
             <div class="input-group-prepend">
                 <span class="input-group-text"  id="basic-addon3">First Name</span>
            </div>
            <input type="text" class="form-control" onChange={this.handleFirst}   placeholder={this.state.userinfo.firstname} value={this.state.first} aria-label="Recipient's username" aria-describedby="basic-addon2"/>
            </div>
            
            <div class="input-group mb-3">
             <div class="input-group-prepend">
                 <span class="input-group-text" id="basic-addon3">Last Name</span>
            </div>
            <input type="text" class="form-control" onChange={this.handleLast} value={this.state.last} placeholder={this.state.userinfo.lastname} aria-label="Recipient's username" aria-describedby="basic-addon2"/>
            </div>
           
            <div class="input-group mb-3">
             <div class="input-group-prepend">
                 <span class="input-group-text"  id="basic-addon3">Password</span>
            </div>
            <input type="password" class="form-control" onChange={this.handlePassword} value={this.state.password} placeholder={this.state.userinfo.userpassword} aria-label="Recipient's username" aria-describedby="basic-addon2"/>
          </div>
          <button type="submit" class="btn btn-primary" onClick={()=>this.update()}>Update</button>
        </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        current_id: state.userId,
    }
}

export default connect(mapStateToProps, null)(withRouter(profile));