import React from 'react';
import axios from 'axios'; 
import { withRouter, Link} from 'react-router-dom';

class register extends React.Component{
    constructor(props) {
        super(props)
        this.state={
            displayfield:false,
            username:"",
            password:"",
            email:"",
            first:"",
            last:"",
        }
        this.handleUsername=this.handleUsername.bind(this);
        this.handlePassword=this.handlePassword.bind(this);
        this.handleEmail=this.handleEmail.bind(this);
        this.handleFirst=this.handleFirst.bind(this);
        this.handleLast=this.handleLast.bind(this);
    }
    handleUsername(event){
        this.setState({
            username:event.target.value
        })
      
    }
    async createUser(){
        if(!this.state.username || !this.state.email || !this.state.first || !this.state.last || !this.state.password){
            window.alert("Please fill in all information")
            this.clearOut();
            return;
        }
        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)){
            window.alert("invalide email format")
            this.clearOut();
            return;
        }
           const checkUser=await axios({
            method: 'post',
            url: 'http://localhost:9025/byusername',
            data:{
                "username": this.state.username,
            },
            withCredentials: true,
             });
            if(checkUser.data){
                window.alert("User Name already exists")
                this.clearOut();
                return;
            }
            await axios({
            method: 'post',
            url: 'http://localhost:9025/newUser',
            data:{
                "username": this.state.username,
                "userpassword": this.state.password,
                "firstname":this.state.first,
                "lastname":this.state.last,
                "email":this.state.email
            },
            withCredentials: true,
             });
             window.alert("Register successful!")
             this.clearOut();
    }
    clearOut(){
        this.setState({
            username:"",
            password:"",
            email:"",
            first:"",
            last:""
        })
    }
    handlePassword(event){
        this.setState({
            password:event.target.value
        })
    }
    handleEmail(event){
        this.setState({
            email:event.target.value
        })  
    }
    handleFirst(event){
        this.setState({
            first:event.target.value
        })  
    }
    handleLast(event){
        this.setState({
            last:event.target.value
        })
        
    }
    getField(){
        this.setState({
            displayfield:true
        })
    }
    render(){
        return(
            <div className="form">
            <h1 onClick={()=>this.createUser()}>Create User</h1>
            <div class="input-group mb-3">
             <div class="input-group-prepend">
                 <span class="input-group-text" id="basic-addon3">User Name</span>
            </div>
            <input type="text" onChange={this.handleUsername} value={this.state.username} class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
            </div>
           <div class="input-group mb-3">
             <div class="input-group-prepend">
                 <span class="input-group-text" id="basic-addon3">Email</span>
            </div>
            <input type="text"  onChange={this.handleEmail}  value={this.state.email} class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
            <div class="input-group-append">
                <select name="email" >
                <option></option>
                <option>@gmail.com</option>
                <option>@yahoo.com</option>
                <option>@163.com</option>
                </select>
            </div>
            </div>
            {this.state.displayfield && 
            <div>
            <div class="input-group mb-3">
             <div class="input-group-prepend">
                 <span class="input-group-text"  id="basic-addon3">First Name</span>
            </div>
            <input type="text" class="form-control" onChange={this.handleFirst}   value={this.state.first} aria-label="Recipient's username" aria-describedby="basic-addon2"/>
            </div>
            
            <div class="input-group mb-3">
             <div class="input-group-prepend">
                 <span class="input-group-text" id="basic-addon3">Last Name</span>
            </div>
            <input type="text" class="form-control" onChange={this.handleLast} value={this.state.last} aria-label="Recipient's username" aria-describedby="basic-addon2"/>
            </div>
           
            <div class="input-group mb-3">
             <div class="input-group-prepend">
                 <span class="input-group-text"  id="basic-addon3">Password</span>
            </div>
            <input type="password" class="form-control" onChange={this.handlePassword} value={this.state.password} aria-label="Recipient's username" aria-describedby="basic-addon2"/>
          </div>
          </div>
            }
            
            <button type="submit" class="btn btn-primary" onClick={()=>this.clearOut()}>Cancel</button>
            <Link to="/"><p>Back to Login</p></Link>
            <footer  onClick={()=>this.getField()} >@More Information</footer>
        
     
         </div>
        )
    }

}
export default withRouter(register)