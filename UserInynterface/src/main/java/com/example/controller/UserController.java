package com.example.controller;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.UserDao;
import com.example.model.Users;

@RestController
@Transactional
@CrossOrigin(origins = "*", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
public class UserController {
	 private UserDao user;
	 
	 @GetMapping("/alluser")
		public List<Users> allUser() {
			return user.findAll();
		}
	 
	 @PostMapping("/byusername")
		public Users username(@RequestBody Users findUser) {
			return user.findByUsername(findUser.getUsername());
		}
	
	 @PostMapping("/byid")
		public Users userid(@RequestBody Users findUser) {
			return user.findByUserid(findUser.getUserid());
		}
	 
	 @PostMapping("/bycred")
		public Users cred(@RequestBody Users findUser) {
			return user.findByUsernameAndUserpassword(findUser.getUsername(), findUser.getUserpassword());
		}
	 
	 @PostMapping("/newUser")
	 public Users newUser(@RequestBody Users newUser) {
		 return user.save(newUser);
	 }
	 
	 @PutMapping("/updateUser")
	 public Users updateUser(@RequestBody Users newUser) {
		 return user.save(newUser);
	 }
	 
	 
	 
	 
	public UserController(UserDao user) {
		super();
		this.user = user;
	}

	public UserDao getUser() {
		return user;
	}

	public void setUser(UserDao user) {
		this.user = user;
	}
	 
	 
}
