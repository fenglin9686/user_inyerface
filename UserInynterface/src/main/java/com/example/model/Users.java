package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class Users {
	@Id
	@Column(name="user_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userid;
	
	@Column(name="user_name")
	private String username;
	
	@Column(name="user_password")
	private String userpassword;
	
	@Column(name="first_name")
	private String firstname;
	
	@Column(name="last_name")
	private String lastname;
	
	@Column(name="email")
	private String email;

	 public Users() {
	
	 }
	public Users(int userid, String username, String userpassword, String firstname, String lastname, String email) {
		super();
		this.userid = userid;
		this.username = username;
		this.userpassword = userpassword;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
	}
	
	public Users(int userid) {
		super();
		this.userid = userid;
	}
	
	public Users(String username, String userpassword, String firstname, String lastname, String email) {
		super();
		this.username = username;
		this.userpassword = userpassword;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
	}
	
	public Users(String username) {
		super();
		this.username = username;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserpassword() {
		return userpassword;
	}
	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Users [userid=" + userid + ", username=" + username + ", userpassword=" + userpassword + ", firstname="
				+ firstname + ", lastname=" + lastname + ", email=" + email + "]";
	}
	
	
	
	
}
