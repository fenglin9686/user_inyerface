package com.example.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.model.Users;


public interface UserDao extends JpaRepository<Users, Integer> {

	 Users findByUsername(String username);
	 Users findByUserid(int id);
	 Users findByUsernameAndUserpassword(String username, String password);
}
